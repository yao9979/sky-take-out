package com.sky.mapper.admin;

import com.sky.annotation.AutoFill;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.enumeration.OperationType;
import com.sky.pojo.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CateGoryMapper {
    /**
     * 分类分页查询
     *
     * @param dto
     * @return
     */
    List<Category> selectCateGoryByPage(@Param("d") CategoryPageQueryDTO dto);

    /**
     * 添加分类
     *
     * @param category
     * @return
     */
    @AutoFill(value = OperationType.INSERT)
    int insertCateGory(Category category);

    /**
     * 启用禁用分类
     *
     * @param
     * @return
     */
    @AutoFill(value =OperationType.UPDATE)
    int updateCateGoryStatus(Category category);

    /**
     * 修改分类
     *
     * @param category
     * @return
     */
    @AutoFill(value =OperationType.UPDATE)
    int updateCateGory(Category category);

    /**
     * 根据id删除分类
     *
     * @param id
     * @return
     */
    @Delete("delete from category where id =#{id}")
    int deleteCateGory(Integer id);

    /**
     * 根据分类类型查询分类
     * @param type
     * @return
     */
    List<Category> selectCateGoryByType(Integer type);




}
