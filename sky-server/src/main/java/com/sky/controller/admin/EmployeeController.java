package com.sky.controller.admin;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.exception.PasswordEditFailedException;
import com.sky.pojo.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.Result;
import com.sky.service.admin.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 员工管理
 */
@RestController
@RequestMapping("/admin/employee")
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JwtProperties jwtProperties;

    /**
     * 登录
     *
     * @param employeeLoginDTO
     * @return
     */
    @PostMapping("/login")
    public Result<EmployeeLoginVO> login(@RequestBody EmployeeLoginDTO employeeLoginDTO) {
        log.info("员工登录：{}", employeeLoginDTO);

        Employee employee = employeeService.login(employeeLoginDTO);

        //登录成功后，生成jwt令牌
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID, employee.getId());
        String token = JwtUtil.createJWT(
                jwtProperties.getAdminSecretKey(),
                jwtProperties.getAdminTtl(),
                claims);

        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .userName(employee.getUsername())
                .name(employee.getName())
                .token(token)
                .build();

        return Result.success(employeeLoginVO);
    }

    /**
     * 退出
     *
     * @return
     */
    @PostMapping("/logout")
    public Result<String> logout() {
        return Result.success();
    }

    @GetMapping("page")
    public Result selectEmpByCondition(EmployeePageQueryDTO dto) {
        return employeeService.selectEmpByCondition(dto);
    }

    @PostMapping
    public Result insertEmp(@RequestBody EmployeeDTO dto) {
        int flag = employeeService.insertEmp(dto);
        return flag == 1 ? Result.success() : Result.error("新增失败");
    }

    @PostMapping("/status/{status}")
    public Result updateEmpStatus(@PathVariable("status") Integer status,Long id){
        int flag = employeeService.updateEmpStatus(status,id);
        return flag == 1 ? Result.success("操作成功") : Result.error("操作失败");
    }

    @GetMapping("/{id}")
    public Result selectById(@PathVariable Long id){
        Employee employee = employeeService.selectEmpById(id);
        return Result.success(employee);
    }
    @PutMapping
    public Result updateEmp(@RequestBody Employee employee){
        int flag = employeeService.updateEmp(employee);
        return flag == 1 ? Result.success("更新成功") : Result.error("更新失败");
    }

    @PutMapping("editPassword")
    public Result updateEmpPassword(@RequestBody PasswordEditDTO dto){
        int flag = employeeService.updateEmpPassword(dto);
        if (flag != 1){
            throw new PasswordEditFailedException("修改密码失败");
        }
        return Result.success("修改成功");

    }


}
