package com.sky.exception;

public class EmployeeUserNameExistException extends BaseException{

    public EmployeeUserNameExistException() {
    }

    public EmployeeUserNameExistException(String msg) {
        super(msg);
    }
}
