package com.sky.mapper.admin;

import com.sky.annotation.AutoFill;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.enumeration.OperationType;
import com.sky.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select * from employee where username = #{username}")
    Employee getByUsername(String username);

    /**
     *分页查询员工
     * @param dto
     * @return
     */
    List<Employee> selectEmpByContidion(@Param("d")EmployeePageQueryDTO dto);

    /**
     * 新增员工
     * @param employee
     * @return
     */
    @AutoFill(value = OperationType.INSERT)
    int insertEmp(@Param("e")Employee employee);

    /**
     * 启动禁用员工状态
     * @param status
     * @return
     */
    @AutoFill(value = OperationType.UPDATE)
    int updateEmpStatus(@Param("status") Integer status,@Param("id") Long id);

    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    @Select("select * from employee where id = #{id}")
    Employee selectEmpById(Long id);

    /**
     * 修改员工信息
     * @param employee
     * @return
     */
    @AutoFill(value = OperationType.UPDATE)
    int updateEmp(Employee employee);

    /**
     * 修改员工密码
     * @param dto
     * @return
     */
    int updateEmpPassword(@Param("dto") PasswordEditDTO dto);

}
