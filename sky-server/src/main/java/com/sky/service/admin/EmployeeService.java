package com.sky.service.admin;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.pojo.Employee;
import com.sky.result.Result;

public interface EmployeeService {

    /**
     * 员工登录
     * @param employeeLoginDTO
     * @return
     */
    Employee login(EmployeeLoginDTO employeeLoginDTO);

    /**
     * 根据条件查询员工信息
     *
     * @param dto
     * @return
     */
    Result selectEmpByCondition(EmployeePageQueryDTO dto);

    /**
     * 新增员工
     * @param dto
     * @return
     */
    int insertEmp(EmployeeDTO dto);

    /**
     * 启动禁用员工状态
     * @param status
     * @return
     */
    int updateEmpStatus(Integer status,Long id);

    /**
     * 根据id查询员工信息
     * @param id
     * @return
     */
    Employee selectEmpById(Long id);

    /**
     * 修改员工信息
     * @param employee
     * @return
     */
    int updateEmp(Employee employee);

    /**
     * 修改员工密码
     * @param dto
     * @return
     */
    int updateEmpPassword(PasswordEditDTO dto);

}
