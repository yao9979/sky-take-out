package com.sky.service.admin.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.exception.*;
import com.sky.pojo.Employee;
import com.sky.mapper.admin.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.admin.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service

public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger log = LoggerFactory.getLogger(EmployeeServiceImpl.class);
    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    @Override
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //1、根据用户名查询数据库中的数据
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        //进行md5加密，然后再进行比对
        if (!DigestUtils.md5DigestAsHex(password.getBytes()).equals(employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus().equals(StatusConstant.DISABLE)) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

    @Override
    public Result selectEmpByCondition(EmployeePageQueryDTO dto) {
        Page<Object> page = PageHelper.startPage(dto.getPage(), dto.getPageSize());
        List<Employee> list = employeeMapper.selectEmpByContidion(dto);
        PageResult pageResult = new PageResult();
        pageResult.setTotal(page.getTotal());
        pageResult.setRecords(list);
        return Result.success(pageResult);
    }

    @Override
    public int insertEmp(EmployeeDTO dto) {
        Employee byUsername = employeeMapper.getByUsername(dto.getUsername());
        if (byUsername != null) {
            throw new EmployeeUserNameExistException(dto.getUsername() + "已存在");
        }
        Employee employee = new Employee();
        BeanUtils.copyProperties(dto, employee);
        //默认状态为启用
        employee.setStatus(StatusConstant.ENABLE);
        //默认密码为123456(加密)
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));
        //先默认一个假数据后去修改
        return employeeMapper.insertEmp(employee);
    }

    @Override
    public int updateEmpStatus(Integer status,Long id) {
        return employeeMapper.updateEmpStatus(status,id);
    }

    @Override
    public Employee selectEmpById(Long id) {
        return employeeMapper.selectEmpById(id);
    }

    @Override
    public int updateEmp(Employee employee) {
        employee.setStatus(StatusConstant.ENABLE);
        return employeeMapper.updateEmp(employee);
    }

    @Override
    public int updateEmpPassword(PasswordEditDTO dto) {
        dto.setEmpId(BaseContext.getCurrentId());
        Employee employee = employeeMapper.selectEmpById(dto.getEmpId());
        if (!employee.getPassword().equals(DigestUtils.md5DigestAsHex(dto.getOldPassword().getBytes()))){
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }
        dto.setNewPassword(DigestUtils.md5DigestAsHex(dto.getNewPassword().getBytes()));
        return employeeMapper.updateEmpPassword(dto);

    }
}
