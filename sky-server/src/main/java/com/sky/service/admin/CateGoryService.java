package com.sky.service.admin;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.pojo.Category;
import com.sky.result.Result;

import java.util.List;

public interface CateGoryService {
    /**
     * 分类分页查询
     * @param dto
     * @return
     */
    Result selectCateGoryByPage(CategoryPageQueryDTO dto);

    /**
     * 新增分类
     * @param dto
     * @return
     */
    int insertCateGory(CategoryDTO dto);

    /**
     * 启用禁用分类
     * @param id
     * @return
     */
    int updateCateGoryStatus(Long id,Integer status);

    /**
     * 修改分类
     * @param dto
     * @return
     */
    int updateCateGory(CategoryDTO dto);

    /**
     * 根据id删除分类
     * @param id
     * @return
     */
    int deleteCateGory(Integer id);

    /**
     * 根据分类类型查询分类
     * @param type
     * @return
     */
    List<Category> selectCateGoryByType(Integer type);
}
