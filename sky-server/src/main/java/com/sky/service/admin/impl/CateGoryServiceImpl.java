package com.sky.service.admin.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.mapper.admin.CateGoryMapper;
import com.sky.pojo.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.admin.CateGoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class CateGoryServiceImpl implements CateGoryService {


    @Resource
    CateGoryMapper cateGoryMapper;

    @Override
    public Result selectCateGoryByPage(CategoryPageQueryDTO dto) {
        Page<Object> page = PageHelper.startPage(dto.getPage(), dto.getPageSize());
        List<Category> categories = cateGoryMapper.selectCateGoryByPage(dto);
        PageResult pageResult = new PageResult();
        pageResult.setTotal(pageResult.getTotal());
        pageResult.setRecords(categories);
        return Result.success(pageResult);
    }

    @Override
    public int insertCateGory(CategoryDTO dto) {
        Category category = new Category();
        BeanUtils.copyProperties(dto, category);
        category.setStatus(StatusConstant.DISABLE);
        return cateGoryMapper.insertCateGory(category);
    }

    @Override
    public int updateCateGoryStatus(Long id, Integer status) {
        Category category = new Category();
        category.setId(id);
        category.setStatus(status);
        return cateGoryMapper.updateCateGoryStatus(category);
    }

    @Override
    public int updateCateGory(CategoryDTO dto) {
        Category category = new Category();
        BeanUtils.copyProperties(dto, category);
        category.setStatus(StatusConstant.DISABLE);
        return cateGoryMapper.updateCateGory(category);
    }

    @Override
    public int deleteCateGory(Integer id) {
        return cateGoryMapper.deleteCateGory(id);
    }

    @Override
    public List<Category> selectCateGoryByType(Integer type) {
        return cateGoryMapper.selectCateGoryByType(type);

    }
}
