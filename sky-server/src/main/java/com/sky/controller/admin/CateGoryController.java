package com.sky.controller.admin;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.pojo.Category;
import com.sky.result.Result;
import com.sky.service.admin.CateGoryService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/category")
public class CateGoryController {

    @Resource
    CateGoryService cateGoryService;

    @GetMapping("/page")
    public Result selectCateGoryByPage(CategoryPageQueryDTO dto) {
        return cateGoryService.selectCateGoryByPage(dto);
    }

    @PostMapping
    public Result insertCateGory(@RequestBody CategoryDTO dto) {
        int flag = cateGoryService.insertCateGory(dto);
        return flag == 1 ? Result.success() : Result.error();
    }

    @PostMapping("/status/{status}")
    public Result updateCateGoryStatus(@PathVariable("status") Integer status, Long id) {
        int flag = cateGoryService.updateCateGoryStatus(id,status);
        return Result.success();
    }

    @PutMapping
    public Result updateCateGory(@RequestBody CategoryDTO dto) {
        int flag = cateGoryService.updateCateGory(dto);
        return flag == 1 ? Result.success("修改成功") : Result.error("修改失败");
    }

    @DeleteMapping
    public Result deleteCateGory(Integer id){
        int flag = cateGoryService.deleteCateGory(id);
        return flag == 1 ? Result.success("删除成功") : Result.error("删除失败");
    }

    @GetMapping("list")
    public Result selectCateGoryByType(Integer type){
        List<Category> categories = cateGoryService.selectCateGoryByType(type);
        return Result.success(categories);
    }


}
